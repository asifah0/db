provider "aws" {
  #shared_credentials_file = "$HOME/.aws/credentials"
  profile                 = "default"
  region                  = "us-east-1"
}

resource "aws_s3_bucket" "b" {
  bucket = "mytestbucket2-123456789"
  acl    = "private"
  policy = file("policy.json")

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
